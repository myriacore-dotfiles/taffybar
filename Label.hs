-- | This module contains some utility functions
-- to do some useful things with labels.

module Label where

import Data.Text as T
import System.Taffybar.Context (TaffybarConfig(..), TaffyIO(..))
import System.Taffybar.Util (truncateText)
import System.Taffybar.Widget.Windows

-- | Truncate Label: Truncates a text Label
truncate :: Int -> T.Text -> T.Text
truncate maxLength = truncateText maxLength

---- Pango Formatting Functions ----
-- These functions format labels according to 
-- pango markup, documented here:
--
-- <https://developer.gnome.org/pango/stable/pango-Markup.html>

bold :: T.Text -> T.Text
bold label = T.concat [ T.pack "<b>", label, T.pack "</b>" ]

big :: T.Text -> T.Text
big label = T.concat [ T.pack "<big>", label, T.pack "</big>" ]

small :: T.Text -> T.Text
small label = T.concat [ T.pack "<small>", label, T.pack "</small>" ]

italic :: T.Text -> T.Text
italic label = T.concat [ T.pack "<i>", label, T.pack "</i>" ]

underline :: T.Text -> T.Text
underline label = T.concat [ T.pack "<u>", label, T.pack "</u>" ]

strike :: T.Text -> T.Text
strike label = T.concat [ T.pack "<s>", label, T.pack "<s>" ]

code :: T.Text -> T.Text
code label = T.concat [ T.pack "<tt>", label, T.pack "</tt>" ]

