#!/bin/bash

# A nice script that allows for hot-reloading of the taffybar 
# without wiping the programs that registered with 
# status-notifier-watcher.

# Kill taffybar if it's already running
pidof myriacore-taffybar && killall -q myriacore-taffybar

# Run status-notifier-watcher if it's not already running
! pidof status-notifier-watcher && status-notifier-watcher

# Run taffybar
myriacore-taffybar >> /tmp/taffybar 2>&1

