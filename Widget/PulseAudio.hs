{-# LANGUAGE OverloadedStrings #-}

-- | This is a pulseaudio widget that 
-- allows you to work with pulseaudio.
module Widget.PulseAudio 
  ( iconPulseAudioNew
  , textPulseAudioNew
  ) where
import System.Taffybar.Widget.Generic.PollingLabel
import Widget.Generic.IconLabel
import Widget.Generic.Clickable
import Control.Monad.IO.Class
import qualified Data.Text as T
import Data.Text (pack, append)
import Data.String (IsString(..))
import System.Process
import GI.Gtk

-- | Creates a PulseAudio icon widget.
-- Left click launches pauvcontrol.
-- Right click toggles mute.
iconPulseAudioNew :: MonadIO m => Double -> m Widget
iconPulseAudioNew interval = 
  pollingIconLabelWidgetNew "<icon>volume-level-medium</icon>" interval
    (const $ getVolume >>= volToIcon)
  >>= (flip clickable) onClick

-- | Creates a PulseAudio text widget. 
-- Left click launches pauvcontrol.
-- Right click toggles mute.
textPulseAudioNew :: MonadIO m => Double -> m Widget
textPulseAudioNew interval =
  pollingLabelNew interval (getVolume >>= volToLabel)
  >>= (flip clickable) onClick

-- | General click handler for PulseAudio widgets.
onClick 
  :: IsWidget widget 
  => widget     -- ^ the child widget to update
  -> ClickEvent -- ^ the click event being handled
  -> IO ()
onClick widget (Single M1) = do -- Launch pavucontrol
  handle <- spawnCommand "pavucontrol"
  return ()
onClick widget (Single M3) = do -- Toggle Mute
  vol' <- toggleMute -- TODO: Find a way to set the new polling Label here
  return ()
onClick _ _ = return () -- Do nothing

-- | Volume(..) represents the volume of a PulseAudio sink.
data Volume = Vol Int | Muted
  deriving (Read, Show)

-- | Renders a Volume type as an emoji. 
-- TODO: Add the % in the label
volToLabel :: IsString str => Volume -> IO str
volToLabel Muted = return "🔇"
volToLabel (Vol n)
  | n >= 66           = return "🔊"
  | 66 > n && n >= 33 = return "🔉"
  | 33 > n && n >= 0  = return "🔈"
  | 0 > n             = return "🔇"

-- | Renders a Volume type as an iconlabel string.
-- TODO: add the % in the label
volToIcon :: IsString str => Volume -> IO str
volToIcon Muted = return "<icon>volume-level-muted</icon>"
volToIcon (Vol n)
  | n >= 66           = return $ "<icon>volume-level-high</icon>"
  | 66 > n && n >= 33 = return $ "<icon>volume-level-medium</icon>"
  | 33 > n && n >= 0  = return $ "<icon>volume-level-low</icon>" 
  | 0 > n             = return $ "<icon>volume-level-muted</icon>"

-- | Gets the volume of the default PulseAudio sink by
-- calling the external program pulsemixer. 
--
-- IMPORTANT: this REQUIRES pulsemixer to work. 
getVolume :: IO Volume
getVolume = do
  lvlStr <- readCreateProcess (shell "pulsemixer --get-volume | cut -d ' ' -f 1") ""
  muteStr <- readCreateProcess (shell "pulsemixer --get-mute") ""
  let lvl = (read lvlStr) :: Int
      muted = toEnum (read muteStr :: Int) :: Bool
  if muted
  then return Muted
  else return (Vol lvl)

toggleMute :: IO Volume
toggleMute =
  callCommand "pulsemixer --toggle-mute"
  >>= const getVolume
