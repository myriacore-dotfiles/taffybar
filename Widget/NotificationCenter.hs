{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
-- | Widget that controls Deadd notification center
--
module Widget.NotificationCenter where

import System.Process
import Control.Monad.IO.Class
import GI.Gtk
import qualified Data.Text as T
import qualified Data.Text.Encoding as E
import qualified Data.ByteString.Lazy as B
import qualified Data.ByteString.Lazy.Char8 as C
import Widget.Generic.IconLabel
import Widget.Generic.Clickable

-- | Creates a text widget for the linux_notification_center. 
-- Left click opens & closes the notification center, and 
-- right click toggles Do not Disturb. 
--
-- NOTE: Do not disturb functinoality only works if you have
-- pause/unpause popup functionality, as implemented here:
-- https://github.com/phuhl/linux_notification_center/pull/65
textNotifCenterNew :: (MonadIO io) => io Widget
textNotifCenterNew =
  let toggleNotifCenter = createProcess (shell "kill -s USR1 $(pidof deadd-notification-center)")
      pauseNotifs = createProcess (shell "notify-send a -h int:deadd-notification-center:1 \
                                          \             -h string:type:pausePopups")
      unpauseNotifs = createProcess (shell "notify-send a -h int:deadd-notification-center:1 \
                                            \             -h string:type:unpausePopups")
      onClick lbl clickEvent = do
        case clickEvent of
          Single M1 -> toggleNotifCenter >> return ()
          Single M3 -> do
            oldText <- labelGetText lbl
            case oldText of 
              "🔔" -> pauseNotifs >> return "🔕" >>= labelSetText lbl
              "🔕" -> unpauseNotifs >> return "🔔" >>= labelSetText lbl
            return ()
          Single MUnknown -> return ()
          _ -> return ()
  in labelNew (Just "🔔") >>= \lbl -> clickable lbl onClick

