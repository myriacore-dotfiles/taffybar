-- | A module that abstracts polling widget logic.
module Widget.Generic.PollingWidget
  ( pollingWidget
  , statefulPollingWidget
  ) where

import Data.IORef (newIORef, readIORef, writeIORef)
import Control.Concurrent ( forkIO, threadDelay )
import Control.Monad ( forever )
import Control.Monad.IO.Class ( MonadIO(..) )
import Control.Exception as E
import qualified GI.Gtk as G
import System.Taffybar.Util

-- | Create a new polling widget.
-- This function is intended to be aliased in %pollingThingNew%-style functions.
-- Note that 'pollingWidget' cannot keep track of state other than the widget
-- that's being updated itself. If you'd like your update function to have
-- access to user-defined state, see @'statefulPollingWidget'@
pollingWidget
  :: (G.IsWidget widget, MonadIO m)
  => widget            -- ^ Widget to be made into a polling widget
  -> Double            -- ^ Polling Period (in seconds)
  -> (widget -> IO ()) -- ^ Polling Callback
  -> m G.Widget
pollingWidget widget period update =
  statefulPollingWidget () widget period (\w _ -> update w)

-- | Create a new stateful polling widget.
-- Intended to be aliased in %pollingThingNew%-style functions. A user-defined
-- initial state will be kept track of, and fed into the update function, which
-- will be used to compute a new state.
statefulPollingWidget
  :: (G.IsWidget widget, MonadIO m)
  => state                         -- ^ Initial State
  -> widget                        -- ^ Widget to be made into a polling widget
  -> Double                        -- ^ Polling Period (in seconds)
  -> (widget -> state -> IO state) -- ^ Update function called each poll
  -> m G.Widget
statefulPollingWidget init widget period update = liftIO $ do
  state <- newIORef init
  _ <- G.onWidgetRealize widget $ do
    _ <- forkIO $ forever $ do
      let update' w = readIORef state >>= update w >>= writeIORef state
          tryUpdate = do
            postGUIASync $ do
              update' widget
              return ()
      E.catch tryUpdate ignoreIOException
      threadDelay $ floor (period * 1000000)
    return ()
  G.toWidget widget
  where ignoreIOException :: IOException -> IO ()
        ignoreIOException = const $ return ()
