-- | This is a simple static image widget, and a polling image widget that
-- updates its contents by calling a callback at a set interval. These 
-- widgets are similar to those provided by System.Taffybar.Widget.Generic.Icon,
-- however these widgets allow for the use of an icon **name**, as opposed
-- to an icon filepath.
--
-- Sourced from:
-- <https://github.com/taffybar/taffybar/blob/a1bec09a7dcc65490eac93163d8cdf55dd1c2cdd/src/System/Taffybar/Widget/Generic/Icon.hs>
module Widget.Generic.Icon 
  ( iconWidgetNew
  , pollingIconWidgetNew
  ) where
import qualified Data.Text as T
import Control.Concurrent ( forkIO, threadDelay )
import Control.Exception as E
import Control.Monad ( forever )
import Control.Monad.IO.Class
import GI.Gtk
import System.Taffybar.Util


-- | Create a new widget that displays a static image
--
-- > iconWidgetNew name
--
-- returns a widget with the icon named @name@.
iconWidgetNew :: MonadIO m => T.Text -> m Widget
iconWidgetNew name = 
  liftIO $ imageNewFromIconName (Just name) (fromIntegral $ fromEnum IconSizeMenu) >>= putInBox

-- | Create a new widget that updates itself at regular intervals.  The
-- function
--
-- > pollingIconWidgetNew name interval cmd
--
-- returns a widget with initial icon whose name is @name@.  The widget
-- forks a thread to update its contents every @interval@ seconds.
-- The command should return the name of a valid icon.
--
-- If the IO action throws an exception, it will be swallowed and the
-- label will not update until the update interval expires.
pollingIconWidgetNew
  :: MonadIO m
  => T.Text    -- ^ Icon Name
  -> Double    -- ^ Update interval (in seconds)
  -> IO T.Text -- ^ Command to run update the icon name
  -> m Widget
pollingIconWidgetNew name interval cmd = liftIO $ do
  icon <- imageNewFromIconName (Just name) (fromIntegral $ fromEnum IconSizeMenu)
  _ <- onWidgetRealize icon $ do
    _ <- forkIO $ forever $ do
      let tryUpdate = do
            str <- cmd
            postGUIASync $ imageSetFromIconName icon (Just str) (fromIntegral $ fromEnum IconSizeMenu)
      E.catch tryUpdate ignoreIOException
      threadDelay $ floor (interval * 1000000)
    return ()
  putInBox icon

-- Sourced from:
-- https://github.com/taffybar/taffybar/blob/a1bec09a7dcc65490eac93163d8cdf55dd1c2cdd/src/System/Taffybar/Widget/Generic/Icon.hs
putInBox :: IsWidget child => child -> IO Widget
putInBox icon = do
  box <- boxNew OrientationHorizontal 0
  boxPackStart box icon False False 0
  widgetShowAll box
  toWidget box

-- Sourced from:
-- https://github.com/taffybar/taffybar/blob/a1bec09a7dcc65490eac93163d8cdf55dd1c2cdd/src/System/Taffybar/Widget/Generic/Icon.hs
ignoreIOException :: IOException -> IO ()
ignoreIOException _ = return ()

