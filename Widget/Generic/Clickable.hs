-- | This module provides ways of producing 
-- GTK widgets that can process click actions.
module Widget.Generic.Clickable 
  ( clickable
  , statefulClickable
  , MouseButton(..)
  , ClickEvent(..)
  ) where

import Data.IORef (newIORef, readIORef, writeIORef)
import Control.Monad.IO.Class
import Data.Word (Word32(..))
import GI.Gtk
import qualified GI.Gdk as Gdk
import qualified Data.Text as T

-- | Datatype representing a click event. 
-- Single is a single click, double is a double click, and triple is a triple
-- click.
data ClickEvent = Single MouseButton | Double MouseButton | Triple MouseButton
     deriving (Read, Show)

-- | Datatype representing mouse buttons. 
-- M1 is typically left click, M2 is typically the middle click, and M3 is right
-- click. On 2 button mice, middle click can often be simulated by pressing both
-- mouse buttons together.
data MouseButton = M1 | M2 | M3 | M4 | M5 | MUnknown
     deriving (Read, Show)

-- | Creates a clickable GTK widget. 
--
-- Users should note that a double click and triple click will *also* send
-- single-clicks.
--
-- Note that this function doesn't preserve state between %onClick%s. If you'd
-- like that functionality, see @'statefulClickable'@
clickable 
  :: (MonadIO m, IsWidget widget) 
  => widget                          -- ^ Original child widget
  -> (widget -> ClickEvent -> IO ()) -- ^ IO action to handle the click event
  -> m Widget
clickable child onClick =
  statefulClickable () child (\w _ c ->  onClick w c)

-- | Creates a stateful clickable GTK widget. 
statefulClickable 
  :: (MonadIO m, IsWidget widget) 
  => state  -- ^ Initial State
  -> widget -- ^ Original child widget
  -> (widget -> state -> ClickEvent -> IO state)
            -- ^ IO action to handle the click event
  -> m Widget
statefulClickable init child onClick = liftIO $ do
  state <- newIORef init
  ebox <- eventBoxNew
  containerAdd ebox child
  eventBoxSetVisibleWindow ebox False
  let handleButtonPress eventButton = do
        mouseButton <- fromWord <$> Gdk.getEventButtonButton eventButton
        eventType <- Gdk.getEventButtonType eventButton
        oldState <- readIORef state
        newState <- onClick child oldState $ fromEventType eventType mouseButton
        writeIORef state newState
        return True
  _ <- onWidgetButtonPressEvent ebox $ handleButtonPress
  widgetShowAll ebox
  toWidget ebox

-- | Creates a ClickEvent from an EventType and a MouseButton. 
fromEventType :: Gdk.EventType -> MouseButton -> ClickEvent
fromEventType eventType mouseButton = 
  case eventType of 
    Gdk.EventTypeButtonPress -> Single mouseButton
    Gdk.EventType2buttonPress -> Double mouseButton
    Gdk.EventType3buttonPress -> Triple mouseButton
    _ -> Single MUnknown

-- | Creates a MouseButton from a Word32. 
fromWord :: Word32 -> MouseButton
fromWord 1 = M1
fromWord 2 = M2
fromWord 3 = M3
fromWord 4 = M4
fromWord 5 = M5
fromWord _ = MUnknown
