{-# LANGUAGE OverloadedStrings #-}
-- | This is a simple weather widget that
-- polls wttr.in to retrieve the weather, instead
-- of relying on noaa data. 

module Widget.Wttr 
  ( textWttrNew 
  , iconWttrNew
  ) where
import System.IO (hPutStrLn, stderr)
import Control.Exception as E
import Control.Monad.IO.Class
import GI.Gtk
import qualified Data.Text as T
import Data.Text.Encoding (decodeUtf8, encodeUtf8)
import Data.ByteString.Lazy (pack, toStrict)
import Data.ByteString (ByteString)
-- import Data.ByteString as B (pack, toStrict, ByteString)
import Network.HTTP.Conduit
import System.Taffybar.Widget.Generic.PollingLabel
import Widget.Generic.IconLabel

-- | Creates a GTK Label widget that polls
-- the requested wttr.in url for weather
-- information. 
--
-- %url% must be properly URL-encoded!
-- This means literal %'s must be used
-- as %25.
-- %interval% is the update interval. 
textWttrNew :: MonadIO m => String -> Double -> m Widget
textWttrNew url interval = pollingLabelNew interval (callWttr url)


-- | Creates a GTK Widget that polls the requested
-- wttr.in url for weather information. This widget
-- will replace any relevant emojis it can find with
-- GTK Icons. 
iconWttrNew 
  :: MonadIO m 
  => String   -- ^ wttr.in url
  -> Double   -- ^ polling interval (in seconds)
  -> m Widget
iconWttrNew url interval = 
  let emojiReplacements = [ ("✨", "<icon>weather-none-available</icon>")
                          , ("☁️", "<icon>weather-overcast</icon>")
                          , ("🌫", "<icon>weather-fog</icon>")
                          , ("🌧", "<icon>weather-showers</icon>")
                          , ("🌦", "<icon>weather-showers-scattered</icon>")
                          , ("❄️", "<icon>weather-snow</icon>")
                          , ("🌨", "<icon>weather-snow-scattered</icon>")
                          , ("⛅️", "<icon>weather-clouds</icon>")
                          , ("☀️", "<icon>weather-clear</icon>")
                          , ("⛈", "<icon>weather-storm</icon>")
                          , ("🌩", "<icon>weather-storm-symbolic</icon>") ]
      replaceEmojis text = 
        foldl (flip $ uncurry T.replace) text emojiReplacements
      update = (const $ replaceEmojis <$> callWttr url)
  in pollingIconLabelWidgetNew "<icon>weather</icon>" interval update

-- TODO: detailed info on hover/click

-- | IO Action that calls 
-- wttr.in as per the user's request. 
-- %url% must be properly URL-encoded!
-- This means literal %'s must be used
-- as %25.
callWttr :: String -> IO T.Text
callWttr url = do
 let unknownLocation rsp = 
       case T.stripPrefix "Unknown location; please try" rsp of
         Nothing          -> False
         Just strippedRsp -> T.length strippedRsp < T.length rsp
 response <- decodeUtf8 <$> E.catch (toStrict <$> simpleHttp url) logException
 if unknownLocation response
 then return $ "✨"
 else return $ response

-- Logs an Http Exception and returns the weather unknown label.
logException :: HttpException -> IO ByteString
logException e = do
  let errmsg = show e
  hPutStrLn stderr ("Warning: Couldn't call wttr.in. \n" ++ errmsg)
  return $ encodeUtf8 "✨"

