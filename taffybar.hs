{-# LANGUAGE OverloadedStrings #-}
module Main where
-- http://hackage.haskell.org/package/taffybar-2.0.0/docs/System-Taffybar.html

import GHC.Int
import GI.Gtk (imageNewFromIconName, Widget, IsWidget, boxNew, boxPackStart, widgetShowAll, toWidget, Orientation(..))
import Control.Monad.IO.Class
import qualified Data.Text as T
import System.Taffybar
import System.Taffybar.SimpleConfig
import System.Taffybar.Widget.Decorators (buildPadBox)
import System.Taffybar.Widget.SNITray
import System.Taffybar.Widget.Workspaces
import System.Taffybar.Widget.Windows
import System.Taffybar.Widget.SimpleClock
import System.Taffybar.Widget.Battery
import Widget.Generic.Icon
import Widget.Generic.IconLabel
import Widget.Generic.Box as B
import Widget.Wttr
import Widget.NotificationCenter
import Widget.PulseAudio
import qualified Label as L

-- | The xinerama/xrandr monitor number to put the bar on (default: PrimaryMonitor)
-- monitorsAction :: TaffyIO [Int]

---- Taffybar Config ----

taffybarConfig :: SimpleTaffyConfig
taffybarConfig = 
  let clock = textClockNew Nothing "📅 %b %_d 🕐 %_H:%M" 1
      tray = sniTrayNew >>= buildPadBox >>= buildPadBox
      -- TODO: try to render a little bar under active workspace. The default
      --       config doesn't do that on its own
      workspaces = workspacesNew defaultWorkspacesConfig { maxIcons = Just 1 }
      wttr = iconWttrNew "http://wttr.in/?0&T&Q&format=%25c+%25f" 10
      windowsConfig = defaultWindowsConfig 
                      { getActiveLabel = L.big . (L.truncate 35) <$> defaultGetActiveLabel }
      audio = iconPulseAudioNew 1
      windows = windowsNew windowsConfig
      battery = sequence [ batteryIconNew >>= buildPadBox >>= buildPadBox, textBatteryNew "$percentage$%" ] >>= B.boxNew
      notifCenter = textNotifCenterNew
  in defaultSimpleTaffyConfig
     { startWidgets = [ workspaces ]
     , centerWidgets = [ windows ]
     -- , endWidgets = [ tray, battery, wttr, clock ]
     , endWidgets = [ tray, notifCenter, audio, battery, wttr, clock ]
     }

main = do
  simpleTaffybar taffybarConfig
