# Taffybar

![Taffybar](screenshots/taffybar.png "Taffybar")

A cute little taffybar built for [my dotfiles](https://gitlab.com/myriacore-dotfiles)!

## Installation

Currently, only source installs are supported. I'm considering including a PKGBUILD for it, though. 

To install from source, you must have a functional [stack](https://docs.haskellstack.org/) installation:

```
$ git clone https://gitlab.com/myriacore-dotfiles/taffybar.git
$ cd taffybar
$ stack install
```

