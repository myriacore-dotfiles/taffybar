import gi
gi.require_version("Gtk", "3.0")
from gi.repository import GLib, Gtk, Gio
from gi.repository.GdkPixbuf import Pixbuf


icons = [ 
  "weather-none-available",
  "weather-overcast",
  "weather-fog",
  "weather-showers",
  "weather-showers-scattered",
  "weather-snow",
  "weather-snow-scattered",
  "weather-clouds",
  "weather-clear",
  "weather-storm",
  "weather-storm-symbolic"
]


class IconViewWindow(Gtk.Window):

    def __init__(self):

        Gtk.Window.__init__(self)
        self.set_title("%d icon%c" % (len(icons), '' if len(icons) < 2 else 's'))
        self.set_default_size(660, 400)

        liststore = Gtk.ListStore(Pixbuf, str)
        box = Gtk.Box.new(Gtk.Orientation.VERTICAL, 10)
        iconview = Gtk.IconView.new()
        iconview.set_model(liststore)
        iconview.set_pixbuf_column(0)
        iconview.set_text_column(1)

        for icon in sorted(icons):
            try:
                # pixbuf = Gtk.IconTheme.get_default().load_icon(icon, 64, 0)
                box.pack_start(Gtk.Image.new_from_icon_name(icon, 64), False, False, 0)
                # box.pack_start(Gtk.Image.new_from_pixbuf(pixbuf), False, False, 0)
                # liststore.append([pixbuf, icon])
            except Exception as inst:
                print(inst)

        swnd = Gtk.ScrolledWindow()
        # swnd.add(iconview)
        swnd.add(box)
        self.add(swnd)


win = IconViewWindow()
win.connect("delete-event", Gtk.main_quit)
win.show_all()
Gtk.main()

